function getTestCanvas1(id) {
    const canvasTest = document.getElementById(id);
    const ctxTest = canvasTest.getContext("2d");
    // canvasTest.width = 1200;
    // canvasTest.height = 300;
    ctxTest.fillStyle = `#FFFFFF`;
    ctxTest.fillRect(0, 0, ctxTest.canvas.width, ctxTest.canvas.height);
    ctxTest.lineWidth = 1;
    ctxTest.beginPath();
    ctxTest.strokeStyle = `#CCCCCC`;
    ctxTest.moveTo(50, 14);
    ctxTest.lineTo(1190, 14);
    ctxTest.moveTo(50, 64);
    ctxTest.lineTo(1190, 64);
    ctxTest.moveTo(50, 114);
    ctxTest.lineTo(1190, 114);
    ctxTest.moveTo(50, 164);
    ctxTest.lineTo(1190, 164);
    ctxTest.moveTo(50, 214);
    ctxTest.lineTo(1190, 214);
    ctxTest.moveTo(50, 264);
    ctxTest.lineTo(1190, 264);
    ctxTest.stroke();



    ctxTest.font = "16px Arial";
    ctxTest.fillStyle = `#CCCCCC`;
    ctxTest.strokeStyle = `#F1F1F1`;
    ctxTest.lineWidth = 1;
    ctxTest.textAlign = "left";
    ctxTest.fillText("1000", 0, 20);
    ctxTest.fillText("800", 10, 70);
    ctxTest.fillText("600", 10, 120);
    ctxTest.fillText("400", 10, 170);
    ctxTest.fillText("200", 10, 220);
    ctxTest.fillText("0", 30, 270);
    ctxTest.fillText("Nov", 135, 300);
    ctxTest.fillText("Dec", 270, 300);
    ctxTest.fillText("Jan", 405, 300);
    ctxTest.fillText("Feb", 540, 300);
    ctxTest.fillText("Mar", 675, 300);
    ctxTest.fillText("Apr", 810, 300);
    ctxTest.fillText("May", 945, 300);
    ctxTest.fillText("Jun", 1080, 300);



    ctxTest.lineWidth = 1;
    ctxTest.beginPath();
    ctxTest.strokeStyle = `#3A80BA`;
    ctxTest.lineWidth = 7;
    ctxTest.lineJoin = 'round';

    ctxTest.moveTo(60, 150);
    ctxTest.lineTo(70, 115);
    ctxTest.lineTo(150, 120);
    ctxTest.lineTo(300, 100);
    ctxTest.lineTo(400, 235);
    ctxTest.lineTo(500, 145);
    ctxTest.lineTo(600, 105);
    ctxTest.lineTo(870, 75);
    ctxTest.lineTo(900, 205);
    ctxTest.lineTo(1070, 112);
    ctxTest.lineTo(1150, 108);
    ctxTest.stroke();

    ctxTest.beginPath();
    ctxTest.fillStyle = '#3A80BA';
    ctxTest.scale(1, 1);
    ctxTest.arc(60, 150, 8, 0, 2 * Math.PI);
    ctxTest.arc(1150, 108, 8, 0, 2 * Math.PI);
    ctxTest.fill();

}

function getTestCanvas2(id) {
    const canvasTest = document.getElementById(id);
    const ctxTest = canvasTest.getContext("2d");
    // canvasTest.width = 1200;
    // canvasTest.height = 300;
    ctxTest.fillStyle = `#FFFFFF`;
    ctxTest.fillRect(0, 0, ctxTest.canvas.width, ctxTest.canvas.height);
    ctxTest.lineWidth = 1;
    ctxTest.beginPath();
    ctxTest.strokeStyle = `#CCCCCC`;
    ctxTest.moveTo(50, 14);
    ctxTest.lineTo(1190, 14);
    ctxTest.moveTo(50, 64);
    ctxTest.lineTo(1190, 64);
    ctxTest.moveTo(50, 114);
    ctxTest.lineTo(1190, 114);
    ctxTest.moveTo(50, 164);
    ctxTest.lineTo(1190, 164);
    ctxTest.moveTo(50, 214);
    ctxTest.lineTo(1190, 214);
    ctxTest.moveTo(50, 264);
    ctxTest.lineTo(1190, 264);
    // ctxTest.moveTo(50, 314);
    // ctxTest.lineTo(1200, 314);
    ctxTest.stroke();

    // let xPos = canvas.width/2;
    // let yPos = 30;

    ctxTest.font = "16px Arial";
    ctxTest.fillStyle = `#CCCCCC`;
    ctxTest.strokeStyle = `#F1F1F1`;
    ctxTest.lineWidth = 1;
    ctxTest.textAlign = "left";
    ctxTest.fillText("1000", 0, 20);
    ctxTest.fillText("800", 10, 70);
    ctxTest.fillText("600", 10, 120);
    ctxTest.fillText("400", 10, 170);
    ctxTest.fillText("200", 10, 220);
    ctxTest.fillText("0", 30, 270);
    ctxTest.fillText("Nov", 135, 300);
    ctxTest.fillText("Dec", 270, 300);
    ctxTest.fillText("Jan", 405, 300);
    ctxTest.fillText("Feb", 540, 300);
    ctxTest.fillText("Mar", 675, 300);
    ctxTest.fillText("Apr", 810, 300);
    ctxTest.fillText("May", 945, 300);
    ctxTest.fillText("Jun", 1080, 300);

    // ctxTest.beginPath();
    // ctxTest.moveTo(xPos, 0);
    // ctxTest.lineTo(xPos, canvas.height);
    // ctxTest.stroke();

    // ctxTest.textAlign = "right";
    // ctxTest.fillText("right", xPos, yPos );
    // ctxTest.textAlign = "end";
    // ctxTest.fillText("end", xPos, yPos * 2);
    // ctxTest.textAlign = "center";
    // ctxTest.fillText("center", xPos, yPos * 3);
    // ctxTest.textAlign = "left";
    // ctxTest.fillText("left", xPos, yPos * 4);
    // ctxTest.textAlign = "start";
    // ctxTest.fillText("start", xPos, yPos * 5);

    ctxTest.lineWidth = 1;
    ctxTest.beginPath();
    ctxTest.strokeStyle = `#3A80BA`;
    ctxTest.lineWidth = 7;
    ctxTest.lineJoin = 'round';

    ctxTest.moveTo(70, 150);
    ctxTest.lineTo(90, 115);
    ctxTest.lineTo(130, 120);
    ctxTest.lineTo(140, 100);
    ctxTest.lineTo(450, 235);
    ctxTest.lineTo(560, 145);
    ctxTest.lineTo(770, 105);
    ctxTest.lineTo(850, 235);
    ctxTest.lineTo(1080, 105);
    ctxTest.lineTo(1150, 212);
    ctxTest.lineTo(1180, 208);
    ctxTest.stroke();

    ctxTest.beginPath();
    ctxTest.fillStyle = '#3A80BA';
    ctxTest.scale(1, 1);
    ctxTest.arc(70, 150, 8, 0, 2 * Math.PI);
    ctxTest.arc(1180, 208, 8, 0, 2 * Math.PI);
    ctxTest.fill();

}

getTestCanvas1('canvas1');
getTestCanvas2('canvas2');